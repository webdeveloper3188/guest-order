<?php
/**
 * Guest Order search Module
 *
 * Interview Assignment - Born Group
 *
 * @category  Born Group
 * @package   Born_OrderController
 * @author    Sudheer Kumar Nayak <webdeveloper3188@gmail.com>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Born_OrderController',
    __DIR__
);
