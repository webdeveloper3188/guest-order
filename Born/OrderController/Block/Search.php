<?php
/**
 * Guest Order search Module
 *
 * Interview Assignment - Born Group
 *
 * @category  Born Group
 * @package   Born_OrderController
 * @author    Sudheer Kumar Nayak <webdeveloper3188@gmail.com>
 */
namespace Born\OrderController\Block;

use Born\OrderController\Model\Order;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Json\Helper\Data;

/**
 * Search block
 */
class Search extends \Magento\Framework\View\Element\Template
{
    /**
     * Guest order model instance
     *
     * @var Born\OrderController\Model\Order
     */
    protected $guestOrderModel;

    /**
     * @var Magento\Framework\Json\Helper\Data
     */
    protected $resultJsonHelper;

    /**
     * @param Magento\Framework\Model\Context $context
     * @param Born\OrderController\Model\Order $guestOrderModel
     * @param Magento\Framework\Json\Helper\Data $resultJsonHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Order $guestOrderModel,
        Data $resultJsonHelper,
        array $data = []
    ){
        $this->guestOrderModel = $guestOrderModel;
        $this->resultJsonHelper = $resultJsonHelper;
        parent::__construct($context, $data);
    }

    /**
     * Return url for guest order search form.
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->getUrl('guestorder/summary');
    }

    /**
     * Return url for guest order search form.
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('guestorder/search');
    }

    /**
     * Return summary of guest order.
     *
     * @return json
     */
    public function getOrderSummary()
    {
        $orderSearchParams = [
            'increment_id' =>  trim($this->getRequest()->getParam('order_id')),
            'customer_email' => trim($this->getRequest()->getParam('email'))
        ];

        /* Check if such order records available in system */
        $filterResult = $this->guestOrderModel->filterGuestOrder($orderSearchParams);
        
        if ($filterResult === true){
            /* Fetch order data in array format */
            $resultPage = $this->guestOrderModel->getOrderData();
            $data = $this->resultJsonHelper->jsonEncode($resultPage);
        } else {
            $data = $this->resultJsonHelper->jsonEncode([
                'error' => 'Order not exists in system, please verify order Id and email-id.'
                ]);
        }
        return $data;
    }
}
