<?php
/**
 * Guest Order search Module
 *
 * Interview Assignment - Born Group
 *
 * @category  Born Group
 * @package   Born_OrderController
 * @author    Sudheer Kumar Nayak <webdeveloper3188@gmail.com>
 */
namespace Born\OrderController\Controller\Search;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Search Index Controller Action
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context, 
        PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return object Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addHandle('guest_order');
        return $resultPage;
    }
}
