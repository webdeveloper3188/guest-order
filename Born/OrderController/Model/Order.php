<?php
/**
 * Guest Order search Module
 *
 * Interview Assignment - Born Group
 *
 * @category  Born Group
 * @package   Born_OrderController
 * @author    Sudheer Kumar Nayak <webdeveloper3188@gmail.com>
 */
namespace Born\OrderController\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Order Model to search guest order and retun matched order data
 */
class Order extends AbstractModel
{
    /**
     * @var Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingHelper;

    /**
     * @var array
     */
    protected $orderData;

    /**
     * @var object Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderCollection;
	
    /**
     * @var Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
		Context $context,
		OrderRepositoryInterface $orderRepository,
		Data $pricingHelper,
		Registry $data,
		SearchCriteriaBuilder $searchCriteriaBuilder
	){
        $this->orderRepository = $orderRepository;
        $this->pricingHelper = $pricingHelper;
		$this->searchCriteriaBuilder = $searchCriteriaBuilder;

        parent::__construct($context, $data);
    }

    /**
     * Return formatted order data as an array
     *
     * @return mixed
     */
    public function getOrderData()
    {
        $this->_setOrderData();
        return $this->orderData;
    }

    /**
     * Filter Order collection by order id and email.
     *
     * @param $orderSearchParams
     * @return bool
     */
    public function filterGuestOrder($orderSearchParams)
    {
        $search_criteria = $this->searchCriteriaBuilder
            ->addFilter("increment_id", $orderSearchParams['increment_id'], "eq")
            ->addFilter("customer_email", $orderSearchParams['customer_email'], "eq")
            ->create();

        $this->_prepareOrderCollection($search_criteria);

        if ($this->orderCollection->count() >= 1){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set order collection by filtering order by search criteria
     *
     * @param $search_criteria
     */
    private function _prepareOrderCollection($search_criteria)
    {
        $this->orderCollection = $this->orderRepository->getList($search_criteria);
    }

    /**
     * Prepare orderData in form of array
     */
    private function _setOrderData()
    {
        foreach ($this->orderCollection->getItems() as $order){
            $this->_prepareOrderDetails($order);
            $this->_prepareOrderProducts($order);
        }
    }

    /**
     * Add product infor of order to orderData
     * @param $order
     */
    private function _prepareOrderProducts(\Magento\Sales\Model\Order $order)
    {
        $this->orderData["ordered_item"]['label'] = __("Products");

        foreach ($order->getItems() as $item) {
            $this->orderData["ordered_item"]['value'][$item->getProductId()]['sku'] = $item->getSku();
            $this->orderData["ordered_item"]['value'][$item->getProductId()]['item_id'] = $item->getId();
            $this->orderData["ordered_item"]['value'][$item->getProductId()]['price_incl_tax'] = $this->pricingHelper->currency($item->getPriceInclTax(), true, false);
        }
    }

    /**
     * Add order basic details to orderData
     * @param $order
     */
    private function _prepareOrderDetails(\Magento\Sales\Model\Order $order)
    {
        $this->orderData['order_status'] = [
            'label' => __("Order Status"),
            'value' => $order->getStatus()
        ];
        
        $this->orderData['total_invoiced'] = [
            'label' => __("Total Invoiced"),
            'value' => $order->getTotalInvoiced()
        ];
        
        $this->orderData['grand_total'] = [
            'label' => __("Order Grand Total"),
            'value' => $order->getGrandTotal()
        ];

        $this->orderData['subtotal'] = [
            'label' => __("Order Subtotal"),
            'value' => $order->getSubtotal()
        ];
    }
}
